#  de.po -- German message strings
#  Copyright (C) 2015  SEIKO EPSON CORPORATION
#
#  License: GPL-3.0+
#  Author : SEIKO EPSON CORPORATION
#
#  This file is part of the 'Utsushi' package.
#  This package is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License or, at
#  your option, any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#  You ought to have received a copy of the GNU General Public License
#  along with this package.  If not, see <http://www.gnu.org/licenses/>.
msgid ""
msgstr ""
"Project-Id-Version: utsushi 0.33.0\n"
"Report-Msgid-Bugs-To: linux-printer@epson.jp\n"
"POT-Creation-Date: 2019-08-19 13:29+0900\n"
"PO-Revision-Date: 2017-11-28 02:42+0900\n"
"Last-Translator: rentaclaus\n"
"Language-Team: German\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.3\n"

#: drivers/combo.cpp:234 drivers/esci/compound-scanner.cpp:2591
#: drivers/esci/compound-scanner.cpp:2626
#: drivers/esci/compound-scanner.cpp:2661
#: drivers/esci/compound-scanner.cpp:2696
#: drivers/esci/extended-scanner.cpp:1267
#: drivers/esci/extended-scanner.cpp:1290
msgid "Resolution"
msgstr "Auflösung"

#: drivers/combo.cpp:242 drivers/esci/compound-scanner.cpp:583
#: drivers/esci/extended-scanner.cpp:443
msgid "Transfer Format"
msgstr "Übertragungsformat"

#: drivers/esci/compound-scanner.cpp:356
msgid "Loading completed"
msgstr "Laden abgeschlossen"

#: drivers/esci/compound-scanner.cpp:357
msgid "Ejecting completed"
msgstr "Auswerfen abgeschlossen"

#: drivers/esci/compound-scanner.cpp:381
msgid "Cleaning is complete."
msgstr "Reinigung abgeschlossen."

#: drivers/esci/compound-scanner.cpp:382
msgid "Calibration is complete."
msgstr "Kalibrierung abgeschlossen."

#: drivers/esci/compound-scanner.cpp:390 drivers/esci/compound-scanner.cpp:415
msgid "Cleaning is failed."
msgstr "Reinigung fehlgeschlagen."

#: drivers/esci/compound-scanner.cpp:391 drivers/esci/compound-scanner.cpp:417
msgid "Calibration is failed."
msgstr "Kalibrierung fehlgeschlagen."

#: drivers/esci/compound-scanner.cpp:411
msgid "Loading failed"
msgstr "Laden fehlgeschlagen"

#: drivers/esci/compound-scanner.cpp:413
msgid "Ejecting failed"
msgstr "Auswerfen fehlgeschlagen"

#: drivers/esci/compound-scanner.cpp:419
msgid "Maintenance failed"
msgstr "Wartung fehlgeschlagen"

#: drivers/esci/compound-scanner.cpp:543
#: drivers/esci/extended-scanner.cpp:1225
msgid "Document Source"
msgstr "Dokumentenquelle"

#: drivers/esci/compound-scanner.cpp:557 drivers/esci/extended-scanner.cpp:331
#: gtkmm/dialog.cpp:798 sane/handle.cpp:592 src/scan-cli.cpp:668
msgid "Image Type"
msgstr "Bildtyp"

#: drivers/esci/compound-scanner.cpp:570
msgid "Dropout"
msgstr "Blindfarbe"

#: drivers/esci/compound-scanner.cpp:584
msgid ""
"Selecting a compressed format such as JPEG normally results in faster device "
"side processing."
msgstr "Die Wahl eines komprimierenden Formats wie z.B. JPEG führt "
"normalerweise zu einer schnelleren Ausführung."

#: drivers/esci/compound-scanner.cpp:598
msgid "JPEG Quality"
msgstr "JPEG Qualität"

#: drivers/esci/compound-scanner.cpp:611 drivers/esci/extended-scanner.cpp:394
#: filters/magick.cpp:184 filters/threshold.cpp:49
msgid "Threshold"
msgstr "Schwellenwert"

#: drivers/esci/compound-scanner.cpp:624 drivers/esci/extended-scanner.cpp:365
msgid "Gamma"
msgstr "Gammawwert"

#: drivers/esci/compound-scanner.cpp:638
#, fuzzy
msgid "Transfer Size"
msgstr "Übertragungsformat"

#: drivers/esci/compound-scanner.cpp:653
msgid "Border Fill"
msgstr "Randfüllung"

#: drivers/esci/compound-scanner.cpp:669
msgid "Left Border"
msgstr "Linker Rand"

#: drivers/esci/compound-scanner.cpp:674
msgid "Right Border"
msgstr "Rechter Rand"

#: drivers/esci/compound-scanner.cpp:679
msgid "Top Border"
msgstr "Oberer Rand"

#: drivers/esci/compound-scanner.cpp:684
msgid "Bottom Border"
msgstr "Unterer Rand"

#: drivers/esci/compound-scanner.cpp:701
msgid "Force Extent"
msgstr "Erzwinge Ausdehnung"

#: drivers/esci/compound-scanner.cpp:702
msgid ""
"Force the image size to equal the user selected size.  Scanners may trim the "
"image data to the detected size of the document.  This may result in images "
"that are not all exactly the same size.  This option makes sure all image "
"sizes match the selected area.\n"
"Note that this option may slow down application/driver side processing."
msgstr ""
"Erzwinge eine Übereinstimmung der Bildgröße mit der gewählten Größe. Scanner "
"können die Bilddaten an die erkannte Doumentengröße anpassen. Dies kann "
"zu Bildern führen welche nicht die genau gleiche Größe haben. Diese Option "
"stellt sicher dass alle Bildgrößen dem gewählten Bereich entsprechen.\n"
"Bitte beachten, diese Option kann die Bearbeitung verlangsamen."

#: drivers/esci/compound-scanner.cpp:743
msgid "Calibration"
msgstr "Kalibrierung"

#: drivers/esci/compound-scanner.cpp:744
msgid "Calibrating..."
msgstr "Kalibrierung läuft..."

#: drivers/esci/compound-scanner.cpp:751
msgid "Cleaning"
msgstr "Reinigung"

#: drivers/esci/compound-scanner.cpp:752
msgid "Cleaning..."
msgstr "Reinigung..."

#: drivers/esci/compound-scanner.cpp:759
msgid "Eject"
msgstr "Ausgabe"

#: drivers/esci/compound-scanner.cpp:760
msgid "Ejecting ..."
msgstr "Auswurf erfolgt ..."

#: drivers/esci/compound-scanner.cpp:767
msgid "Load"
msgstr "Einlegen"

#: drivers/esci/compound-scanner.cpp:768
msgid "Loading..."
msgstr "Laden..."

#: drivers/esci/compound-scanner.cpp:1731
#: drivers/esci/compound-scanner.cpp:2725
#: drivers/esci/extended-scanner.cpp:1314
msgid "Manual"
msgstr "Manuelle Zufuhr"

#: drivers/esci/compound-scanner.cpp:1732
#: drivers/esci/compound-scanner.cpp:2726
#: drivers/esci/extended-scanner.cpp:1315
msgid "Maximum"
msgstr "Maximal"

#: drivers/esci/compound-scanner.cpp:1739
#: drivers/esci/compound-scanner.cpp:2728
#: drivers/esci/compound-scanner.cpp:2795
#: drivers/esci/extended-scanner.cpp:1317
msgid "Auto Detect"
msgstr "Automatische Erkennung"

#: drivers/esci/compound-scanner.cpp:1954
#: drivers/esci/grammar-capabilities.cpp:567
msgid "RAW"
msgstr "RAW"

#: drivers/esci/compound-scanner.cpp:2075
#: drivers/esci/extended-scanner.cpp:1071
#, boost-format
msgid ""
"Scan area too small.\n"
"The area needs to be larger than %1% by %2%."
msgstr "Scanbereich zu klein. \n"
"Der Bereich muss größer als %1% mal %2% sein."

#: drivers/esci/compound-scanner.cpp:2381
#, boost-format
msgid ""
"Resolution too high for selected area.\n"
"Choose a resolution no larger than %1%"
msgstr "Auflösung zu hoch für den gewählten Bereich. \n"
"Eine Auflösung nicht größer als %1% wählen."

#: drivers/esci/compound-scanner.cpp:2428
#: drivers/esci/extended-scanner.cpp:1187
msgid "Duplex"
msgstr "Duplex"

#: drivers/esci/compound-scanner.cpp:2440
msgid "Image Count"
msgstr "Bildzähler"

#: drivers/esci/compound-scanner.cpp:2452
msgid "Detect Double Feed"
msgstr "Doppeleinzugsfehler"

#: drivers/esci/compound-scanner.cpp:2463
msgid "Long Paper Mode"
msgstr "Langpapiermodus"

#: drivers/esci/compound-scanner.cpp:2464
msgid ""
"Select this mode if you want to scan documents longer than what the ADF "
"would normally support.  Please note that it only supports automatic "
"detection of the document height."
msgstr ""
"Diesen Modus wählen um Dokumente zu scannen welche länger sind als der "
"ADF unterstützt. Bitte beachten Sie dass ausschliesslich die automatische "
"Erkennung der Dokumentenhöhe unterstützt wird."

#: drivers/esci/compound-scanner.cpp:2587
#: drivers/esci/compound-scanner.cpp:2657
msgid "Bind X and Y resolutions"
msgstr "X und Y Auflösung koppeln"

#: drivers/esci/compound-scanner.cpp:2595
#: drivers/esci/compound-scanner.cpp:2610
#: drivers/esci/compound-scanner.cpp:2665
#: drivers/esci/compound-scanner.cpp:2680
msgid "X Resolution"
msgstr "X Auflösung"

#: drivers/esci/compound-scanner.cpp:2599
#: drivers/esci/compound-scanner.cpp:2614
#: drivers/esci/compound-scanner.cpp:2669
#: drivers/esci/compound-scanner.cpp:2684
msgid "Y Resolution"
msgstr "Y Auflösung"

#: drivers/esci/compound-scanner.cpp:2644
#: drivers/esci/extended-scanner.cpp:1282
msgid "Enable Resampling"
msgstr "Neuberechnung aktivieren"

#: drivers/esci/compound-scanner.cpp:2645
#: drivers/esci/extended-scanner.cpp:1283
msgid ""
"This option provides the user with a wider range of supported resolutions.  "
"Resolutions not supported by the hardware will be achieved through image "
"processing methods."
msgstr ""
"Diese Option gestattet einen erweiterten Bereich an Auflösungen. "
"Auflösungen welche nicht vom Gerät bereitgestellt werden, werden durch"
"Bildbearbeitung erzielt."

#: drivers/esci/compound-scanner.cpp:2735
#: drivers/esci/extended-scanner.cpp:1324
msgid "Scan Area"
msgstr "Scanbereich"

#: drivers/esci/compound-scanner.cpp:2743
#: drivers/esci/extended-scanner.cpp:1332
msgid "Top Left X"
msgstr "X oben links"

#: drivers/esci/compound-scanner.cpp:2751
#: drivers/esci/extended-scanner.cpp:1348
msgid "Top Left Y"
msgstr "Y oben links"

#: drivers/esci/compound-scanner.cpp:2759
#: drivers/esci/extended-scanner.cpp:1340
msgid "Bottom Right X"
msgstr "X unten rechts"

#: drivers/esci/compound-scanner.cpp:2767
#: drivers/esci/extended-scanner.cpp:1356
msgid "Bottom Right Y"
msgstr "Y unten rechts"

#: drivers/esci/compound-scanner.cpp:2803
msgid "Crop"
msgstr "Zuschneiden"

#: drivers/esci/compound-scanner.cpp:2815
msgid "Crop Adjustment"
msgstr "Anordnung zuschneiden"

#: drivers/esci/compound-scanner.cpp:2839 gtkmm/dialog.cpp:778
#: sane/handle.cpp:545 src/scan-cli.cpp:644
msgid "Deskew"
msgstr "Entzerren"

#: drivers/esci/compound-scanner.cpp:2867
msgid "Overscan"
msgstr "Überschreiben"

#: drivers/esci/compound-scanner.cpp:3272
msgid ""
"Authentication is required.\n"
"Unfortunately, this version of the driver does not support authentication "
"yet."
msgstr ""
"Authentifizierung ist erforderlich.\n"
"Leider unterstützt diese Version des Treibers noch keine Authentifizierung."

#: drivers/esci/compound-scanner.cpp:3276
#, boost-format
msgid "Unknown device error: %1%/%2%"
msgstr "Unbekannter Gerätefehler: %1%/%2%"

#: drivers/esci/compound-scanner.cpp:3366
#: drivers/esci/extended-scanner.cpp:206 sane/backend.cpp:288
msgid ""
"The Automatic Document Feeder is open.\n"
"Please close it."
msgstr ""
"Der automatische Vorlageneinzug ist offen.\n"
"Bitte schließen."

#: drivers/esci/compound-scanner.cpp:3369
#: drivers/esci/extended-scanner.cpp:201 sane/backend.cpp:274
msgid ""
"A paper jam occurred.\n"
"Open the Automatic Document Feeder and remove any paper.\n"
"If there are any documents loaded in the ADF, remove them and load them "
"again."
msgstr ""
"Papierstau. Öffnen Sie den automatischen Vorlageneinzug (ADF) und entfernen "
"Sie das Papier. Befinden sich Dokumente im ADF, entfernen Sie sie und legen "
"Sie sie erneut ein."

#: drivers/esci/compound-scanner.cpp:3374
#: drivers/esci/extended-scanner.cpp:198 sane/backend.cpp:269
msgid "Please load the document(s) into the Automatic Document Feeder."
msgstr "Legen Sie die Vorlage(n) in den automatischen Vorlageneinzug ein."

#: drivers/esci/compound-scanner.cpp:3377
#: drivers/esci/extended-scanner.cpp:209 sane/backend.cpp:281
msgid ""
"A multi page feed occurred in the auto document feeder. Open the cover, "
"remove the documents, and then try again. If documents remain on the tray, "
"remove them and then reload them."
msgstr ""
"Es wurden mehrere Seiten gleichzeitig im automatischem Vorlageneinzug "
"eingezogen. Öffnen Sie die Abdeckung, entfernen Sie die Dokumente und "
"starten Sie den Vorgang erneut. Entfernen Sie vorhandene Dokumente aus dem "
"Fach und legen Sie sie erneut ein."

#: drivers/esci/compound-scanner.cpp:3382
#: drivers/esci/extended-scanner.cpp:214
msgid ""
"A fatal ADF error has occurred.\n"
"Resolve the error condition and try again.  You may have to restart the scan "
"dialog or application in order to be able to scan."
msgstr ""
"Ein schwerer ADF Fehler ist aufgetreten.\n"
"Fehler beheben und erneut versuchen. Es kann nötig sein den Scandialog oder die "
"Anwendung neu zu starten um scannen zu können."

#: drivers/esci/compound-scanner.cpp:3396
msgid "A fatal error has occurred"
msgstr "Ein schwerer Fehler ist aufgetreten"

#: drivers/esci/compound-tweaks.cpp:111 drivers/esci/compound-tweaks.cpp:356
#: drivers/esci/extended-scanner.cpp:335
msgid "Speed"
msgstr "Geschwindigkeit"

#: drivers/esci/compound-tweaks.cpp:112 drivers/esci/compound-tweaks.cpp:357
msgid "Optimize image acquisition for speed"
msgstr "Optimiere Bilderfassung für Geschwindigkeit"

#: drivers/esci/compound.cpp:629
msgid ""
"The device is in use.  Please wait until the device becomes available, then "
"try again."
msgstr ""
"Das Gerät wird gerade verwendet. Bitte warten Sie, bis das Gerät verfügbar "
"wird, und versuchen Sie es dann erneut."

#: drivers/esci/exception.hpp:64
msgid "invalid parameter"
msgstr "Ungültiger Parameter"

#: drivers/esci/exception.hpp:74
msgid "unknown reply"
msgstr "Unbekannte Antwort"

#: drivers/esci/exception.hpp:84
msgid "invalid command"
msgstr "Ungültiger Befehl"

#: drivers/esci/exception.hpp:94
msgid "device busy"
msgstr "Gerät ausgelastet"

#: drivers/esci/exception.hpp:104
msgid "protocol error"
msgstr "Protokollfehler"

#: drivers/esci/extended-scanner.cpp:84
msgid "Positive Film"
msgstr "Filmpositiv"

#: drivers/esci/extended-scanner.cpp:85
msgid "Negative Film"
msgstr "Filmnegativ"

#: drivers/esci/extended-scanner.cpp:100
msgid "Bi-level CRT"
msgstr "Bi-level CRT"

#: drivers/esci/extended-scanner.cpp:101
msgid "Multi-level CRT"
msgstr "Multi-level CRT"

#: drivers/esci/extended-scanner.cpp:102
msgid "High Density Print"
msgstr "Hochauflösender Druck"

#: drivers/esci/extended-scanner.cpp:103
msgid "Low Density Print"
msgstr "Niedrigauflösender Druck"

#: drivers/esci/extended-scanner.cpp:104
msgid "High Contrast Print"
msgstr "Hochkontrast Druck"

#: drivers/esci/extended-scanner.cpp:105
msgid "Custom (Base Gamma = 1.0)"
msgstr "Angepasst (Gamma = 1.0)"

#: drivers/esci/extended-scanner.cpp:106
msgid "Custom (Base Gamma = 1.8)"
msgstr "Angepasst (Gamma = 1.8)"

#: drivers/esci/extended-scanner.cpp:121
#: drivers/esci/grammar-capabilities.cpp:260
#: drivers/esci/grammar-capabilities.cpp:424
msgid "None"
msgstr "Kein"

#: drivers/esci/extended-scanner.cpp:123
msgid "Dot Matrix Printer"
msgstr "Nadeldrucker"

#: drivers/esci/extended-scanner.cpp:124
msgid "Thermal Printer"
msgstr "Thermodrucker"

#: drivers/esci/extended-scanner.cpp:125
msgid "Inkjet Printer"
msgstr "Tintenstrahldrucker"

#: drivers/esci/extended-scanner.cpp:126
msgid "CRT Display"
msgstr "Röhrenbildschirm"

#: drivers/esci/extended-scanner.cpp:141
msgid "Bi-level"
msgstr ""

#: drivers/esci/extended-scanner.cpp:142
msgid "Text Enhanced"
msgstr ""

#: drivers/esci/extended-scanner.cpp:143
msgid "Hard Tone"
msgstr ""

#: drivers/esci/extended-scanner.cpp:144
msgid "Soft Tone"
msgstr ""

#: drivers/esci/extended-scanner.cpp:145
msgid "Net Screen"
msgstr ""

#: drivers/esci/extended-scanner.cpp:146
msgid "Bayer 4x4"
msgstr "Bayer 4x4"

#: drivers/esci/extended-scanner.cpp:147
msgid "Spiral 4x4"
msgstr "Spiral 4x4"

#: drivers/esci/extended-scanner.cpp:148
msgid "Net Screen 4x4"
msgstr ""

#: drivers/esci/extended-scanner.cpp:149
msgid "Net Screen 8x4"
msgstr ""

#: drivers/esci/extended-scanner.cpp:191
msgid "Unknown device error"
msgstr "Unbekannter Gerätefehler"

#: drivers/esci/extended-scanner.cpp:326
#: drivers/esci/grammar-capabilities.cpp:689 gtkmm/dialog.cpp:794
#: sane/handle.cpp:588 src/scan-cli.cpp:664
msgid "Monochrome"
msgstr "Monochrom"

#: drivers/esci/extended-scanner.cpp:327
#: drivers/esci/grammar-capabilities.cpp:690 gtkmm/dialog.cpp:795
#: sane/handle.cpp:589 src/scan-cli.cpp:665
msgid "Grayscale"
msgstr "Graustufen"

#: drivers/esci/extended-scanner.cpp:328
#: drivers/esci/grammar-capabilities.cpp:687 drivers/esci/scanner.cpp:412
#: gtkmm/dialog.cpp:796 sane/handle.cpp:590 src/scan-cli.cpp:666
msgid "Color"
msgstr "Farbe"

#: drivers/esci/extended-scanner.cpp:342
msgid "Line Count"
msgstr ""

#: drivers/esci/extended-scanner.cpp:343
msgid ""
"Specify how many scan lines to move from the device to the software in one "
"transfer.  Note that 0 will use the maximum usable value.  Values larger "
"than the maximum usable value are clamped to the maximum."
msgstr ""

#: drivers/esci/extended-scanner.cpp:357
msgid "Gamma Correction"
msgstr "Gammakorrektur"

#: drivers/esci/extended-scanner.cpp:374
msgid "Color Correction"
msgstr "Farbkorrektur"

#: drivers/esci/extended-scanner.cpp:382
msgid "Auto Area Segmentation"
msgstr "Automatische Einteilung in Bereiche"

#: drivers/esci/extended-scanner.cpp:383
msgid "Threshold text regions and apply half-toning to photo/image areas."
msgstr ""

#: drivers/esci/extended-scanner.cpp:404
msgid "Dither Pattern"
msgstr "Fehlerdiffusionsmuster"

#: drivers/esci/extended-scanner.cpp:414
msgid "Sharpness"
msgstr "Bildschärfe"

#: drivers/esci/extended-scanner.cpp:415
msgid ""
"Emphasize the edges in an image more by choosing a larger value, less by "
"selecting a smaller value."
msgstr ""
"Kanten in einem Bild durch einen höheren Wert betonen, "
"abschwächen mittels kleinerem Wert."

#: drivers/esci/extended-scanner.cpp:426 filters/magick.cpp:191
msgid "Brightness"
msgstr "Helligkeit"

#: drivers/esci/extended-scanner.cpp:427
msgid ""
"Make images look lighter with a larger value or darker with a smaller value."
msgstr ""
"Bilder aufhellen durch höheren Wert, abdunkeln durch kleineren Wert."

#: drivers/esci/extended-scanner.cpp:434
msgid "Mirror"
msgstr "Spiegeln"

#: drivers/esci/extended-scanner.cpp:1174
#: drivers/esci/grammar-capabilities.cpp:330
#: drivers/esci/grammar-capabilities.cpp:340
msgid "Document Table"
msgstr "Vorlagenglas"

#: drivers/esci/extended-scanner.cpp:1180
#: drivers/esci/grammar-capabilities.cpp:328
#: drivers/esci/grammar-capabilities.cpp:338
msgid "ADF"
msgstr "Automatischer Dokumenteneinzug"

#: drivers/esci/extended-scanner.cpp:1199
msgid "Primary TPU"
msgstr "Primäre Durchlichteinheit"

#: drivers/esci/extended-scanner.cpp:1200
msgid "Secondary TPU"
msgstr "Sekundäre Durchlichteinheit"

#: drivers/esci/extended-scanner.cpp:1204
#: drivers/esci/grammar-capabilities.cpp:329
#: drivers/esci/grammar-capabilities.cpp:339
msgid "Transparency Unit"
msgstr "Durchlichtaufsatz"

#: drivers/esci/extended-scanner.cpp:1213
msgid "Film Type"
msgstr "Filmtyp"

#: drivers/esci/grammar-capabilities.cpp:269
msgid "White"
msgstr "Weiß"

#: drivers/esci/grammar-capabilities.cpp:270
msgid "Black"
msgstr "Schwarz"

#: drivers/esci/grammar-capabilities.cpp:369
#: drivers/esci/grammar-capabilities.cpp:384
msgid "Off"
msgstr "Aus"

#: drivers/esci/grammar-capabilities.cpp:370
msgid "Normal"
msgstr "Normal"

#: drivers/esci/grammar-capabilities.cpp:371
msgid "Thin"
msgstr "Dünn"

#: drivers/esci/grammar-capabilities.cpp:385
msgid "On"
msgstr "Ein"

#: drivers/esci/grammar-capabilities.cpp:386
msgid "Paper Protection"
msgstr "Papierschutz"

#: drivers/esci/grammar-capabilities.cpp:474
msgid "Red"
msgstr "Rot"

#: drivers/esci/grammar-capabilities.cpp:475
msgid "Green"
msgstr "Grün"

#: drivers/esci/grammar-capabilities.cpp:476
msgid "Blue"
msgstr "Blau"

#: drivers/esci/grammar-capabilities.cpp:568 gtkmm/file-chooser.cpp:453
msgid "JPEG"
msgstr "JPEG"

#: drivers/esci/scanner.cpp:409
msgid "ADF - Double-sided"
msgstr "ADF-Doppelseitig"

#: drivers/esci/scanner.cpp:410
msgid "ADF - Single-sided"
msgstr "ADF-Einseitig"

#: drivers/esci/scanner.cpp:411
msgid "Black & White"
msgstr "Schwarzweiß"

#: drivers/esci/scanner.cpp:413
msgid "Mode"
msgstr "Modus"

#: drivers/esci/scanner.cpp:414
msgid "Text/Line Art"
msgstr "Text/Strichzeichnung"

#: filters/image-skip.cpp:67
msgid "Skip Blank Pages Settings"
msgstr "Leere Seiten überspringen Einstellungen"

#: filters/jpeg.cpp:229
msgid "Buffer Size"
msgstr "Puffergröße"

#: filters/jpeg.cpp:250
msgid "Image Quality"
msgstr "Bildqualität"

#: filters/magick.cpp:192
msgid "Change brightness of the acquired image."
msgstr "Helligkeit des erfassten Bildes ändern."

#: filters/magick.cpp:199
msgid "Contrast"
msgstr "Kontrast"

#: filters/magick.cpp:200
msgid "Change contrast of the acquired image."
msgstr "Kontrast des erfassten Bildes ändern."

#: filters/reorient.cpp:258
msgid "0 degrees"
msgstr "0°"

#: filters/reorient.cpp:259
msgid "90 degrees"
msgstr "90°"

#: filters/reorient.cpp:260
msgid "180 degrees"
msgstr "180°"

#: filters/reorient.cpp:261
msgid "270 degrees"
msgstr "270°"

#: filters/reorient.cpp:262
msgid "Auto"
msgstr "Automatisch"

#: filters/reorient.cpp:289
msgid "Rotate"
msgstr "Drehen"

#: gtkmm/action-dialog.cpp:112 gtkmm/action-dialog.cpp:119
#: gtkmm/dialog.glade:43
msgid "Maintenance"
msgstr "Wartung"

#: gtkmm/chooser.cpp:70
msgid "No devices found"
msgstr "Keine Geräte gefunden"

#: gtkmm/chooser.cpp:77
msgid "Select a device"
msgstr "Ein Gerät auswählen"

#: gtkmm/chooser.cpp:205
#, boost-format
msgid ""
"Cannot access %1%\n"
"(%2%)\n"
"%3%"
msgstr ""
"Zugriff auf %1%\n"
"(%2%)\n"
"%3% nicht möglich"

#: gtkmm/dialog.cpp:79
msgid "Scanning..."
msgstr "Scanvorgang läuft..."

#: gtkmm/dialog.cpp:80
msgid "Canceling..."
msgstr "Abbrechen..."

#: gtkmm/dialog.cpp:357
msgid "Save As..."
msgstr "Speichern unter..."

#: gtkmm/dialog.cpp:359
msgid "Untitled"
msgstr "Unbenannt"

#: gtkmm/dialog.cpp:658 src/scan-cli.cpp:1135
#, boost-format
msgid "conversion from %1% to %2% is not supported"
msgstr "Umwandlung von %1% in %2% wird nicht unterstützt"

#: gtkmm/dialog.glade:28 gtkmm/dialog.glade:366
msgid "Scan"
msgstr "Scannen"

#: gtkmm/dialog.glade:53
msgid "Details:"
msgstr "Details:"

#: gtkmm/dialog.glade:90
msgid "Preview"
msgstr "Vorschau"

#: gtkmm/dialog.glade:97
msgid "Zoom In"
msgstr "Vorschau vergrößern"

#: gtkmm/dialog.glade:104
msgid "Zoom Out"
msgstr "Vorschau verkleinern"

#: gtkmm/dialog.glade:111
msgid "Actual Size"
msgstr "Vorschau tatsächliche Größe"

#: gtkmm/dialog.glade:118
msgid "Zoom to fit"
msgstr "Vorschaugröße einpassen"

#: gtkmm/dialog.glade:188
msgid "Scanner:"
msgstr "Scanner:"

#: gtkmm/dropdown.cpp:170
msgid "To be implemented."
msgstr ""

#: gtkmm/dropdown.cpp:173
#, boost-format
msgid ""
"Support for changing the active item has not been implemented yet.  Should "
"be changing from\n"
"\n"
"\t<b>%1%</b>\n"
"\n"
"to\n"
"\n"
"\t<b>%2%</b>"
msgstr ""

#: gtkmm/dropdown.cpp:193
#, boost-format
msgid ""
"Support for management action functions has not been implemented yet.  This "
"action could manipulate, and revert to,\n"
"\n"
"\t<b>%1%</b>"
msgstr ""

#: gtkmm/editor.cpp:460
msgid "Other"
msgstr "Sonstiges"

#: gtkmm/editor.cpp:471
#, fuzzy
msgid "Application"
msgstr "Kalibrierung"

#: gtkmm/editor.cpp:682
msgid "Restoring previous value"
msgstr "Vorherigen Wert wiederherstellen"

#: gtkmm/editor.cpp:685
msgid "The selected combination of values is not supported."
msgstr "Die gewählte Kombination von Werten wird nicht unterstützt."

#: gtkmm/file-chooser.cpp:262
msgid "Unsupported file format."
msgstr "Dateiformat nicht unterstützt."

#: gtkmm/file-chooser.cpp:266
#, boost-format
msgid ""
"The '%1%' file extension is not associated with a supported file format.  "
"Please select a file format or use one of the known file extensions."
msgstr ""
"Die Dateierweiterung '%1%' ist keinem unterstützten Dateiformat zugeordnet. "
"Bitte wählen Sie ein Dateiformat aus oder verwenden Sie eine der bekannten "
"Dateierweiterungen."

#: gtkmm/file-chooser.cpp:286
#, boost-format
msgid "The %1% format does not support multiple images in a single file."
msgstr "Das Format %1% unterstützt nicht mehrere Bilder in einer Datei."

#: gtkmm/file-chooser.cpp:291
#, boost-format
msgid ""
"Please save to PDF or TIFF if you want a single file.  If you prefer the %1% "
"image format, use a filename such as 'Untitled-%%3i%2%'."
msgstr ""
"Bitte speichern Sie als PDF oder TIFF, wenn Sie nur eine Datei möchten. "
"Wenn Sie das Bildformat %1% bevorzugen, verwenden Sie bitte einen Dateinamen "
"wie 'Unbenannt-%%3i%2%'."

#: gtkmm/file-chooser.cpp:315
#, boost-format
msgid ""
"The name \"%1%\" already exists.\n"
"OK to overwrite this name using the new settings?"
msgstr ""
"Der Name \"%1%\" existiert bereits.\n"
"Soll dieser Name mit den neuen Einstellungen überschrieben werden?"

#: gtkmm/file-chooser.cpp:318
#, boost-format
msgid ""
"The file already exists in \"%1%\".  Replacing it will overwrite its "
"contents."
msgstr ""
"Die Datei existiert bereits in \"%1%\". Durch Ersetzen wird ihr Inhalt "
"überschrieben."

#: gtkmm/file-chooser.cpp:326
#, boost-format
msgid "Files matching \"%1%\" may already exist.  Do you want to replace them?"
msgstr ""
"Dateien, die \"%1%\" entsprechen, existieren unter Umständen bereits. "
"Möchten Sie sie ersetzen?"

#: gtkmm/file-chooser.cpp:373 gtkmm/file-chooser.cpp:505
#: gtkmm/file-chooser.cpp:636
msgid "File Type"
msgstr "Dateityp"

#: gtkmm/file-chooser.cpp:377
#, boost-format
msgid "File type: %1%"
msgstr "Dateityp: %1%"

#: gtkmm/file-chooser.cpp:449
msgid "By extension"
msgstr "Nach Erweiterung"

#: gtkmm/file-chooser.cpp:462
msgid "PDF"
msgstr "PDF"

#: gtkmm/file-chooser.cpp:471
msgid "PNG"
msgstr "PNG"

#: gtkmm/file-chooser.cpp:480
msgid "PNM"
msgstr "PNM"

#: gtkmm/file-chooser.cpp:489
msgid "TIFF"
msgstr "TIFF"

#: gtkmm/file-chooser.cpp:509
msgid "Save all images in a single file"
msgstr "Alle Bilder in einer Datei speichern"

#: gtkmm/file-chooser.cpp:545
msgid "PDFs and Image Files"
msgstr "PDF- und Bilddateien"

#: gtkmm/file-chooser.cpp:551
msgid "Image Files"
msgstr "Bilddateien"

#: gtkmm/file-chooser.cpp:557
msgid "All Files"
msgstr "Alle Dateien"

#: lib/descriptor.cpp:47
msgid "Standard"
msgstr "Standard"

#: lib/descriptor.cpp:48
msgid ""
"If there is any user interface at all, options at the standard level are "
"meant to be made available to the user."
msgstr ""
"Standard ist für Scanaufgaben in denen entweder keine oder nur "
"geringfügige Benutzerinteraktion benötigt werden."

#: lib/descriptor.cpp:54
msgid "Extended"
msgstr "Erweitert"

#: lib/descriptor.cpp:55
msgid ""
"Extended options are for those situations where the user needs a bit more "
"control over how things will be done."
msgstr ""
"Erweiterte Optionen sind für Scanaufgaben in denen der Benutzer eine erweiterte "
"Kontrolle über das vorgehen benötigt."

#: lib/descriptor.cpp:61
msgid "Complete"
msgstr "Vollständig"

#: lib/descriptor.cpp:62
msgid ""
"This is for options that are mostly just relevant for the most demanding of "
"image acquisition jobs or those users will not be satisfied unless they are "
"in complete control."
msgstr ""
"Vollständige Optionen sind für Scanaufgaben in denen der Benutzer eine komplette "
"Kontrolle über das vorgehen benötigt oder wünscht."

#: lib/media.cpp:62
msgid "ISO/A3"
msgstr "ISO/A3"

#: lib/media.cpp:63
msgid "ISO/A4"
msgstr "ISO/A4"

#: lib/media.cpp:64
msgid "ISO/A5"
msgstr "ISO/A5"

#: lib/media.cpp:65
msgid "ISO/A6"
msgstr "ISO/A6"

#: lib/media.cpp:67
msgid "JIS/B4"
msgstr "JIS/B4"

#: lib/media.cpp:68
msgid "JIS/B5"
msgstr "JIS/B5"

#: lib/media.cpp:69
msgid "JIS/B6"
msgstr "JIS/B6"

#: lib/media.cpp:71
msgid "Ledger"
msgstr "US Ledger"

#: lib/media.cpp:72
msgid "Legal"
msgstr "Legal"

#: lib/media.cpp:73
msgid "Letter"
msgstr "Letter"

#: lib/media.cpp:74
msgid "Executive"
msgstr "US Executive"

#: lib/pump.cpp:83
msgid "Acquire image data asynchronously"
msgstr "Bilddaten asynchron erfassen"

#: lib/pump.cpp:84
msgid ""
"When true, image acquisition will proceed independently from the rest of the "
"program.  Normally, this would be what you want because it keeps the program "
"responsive to user input and updated with respect to progress.  However, in "
"case of trouble shooting you may want to turn this off to obtain a more "
"predictable program flow.\n"
"Note, you may no longer be able to cancel image acquisition via the normal "
"means when this option is set to false."
msgstr ""

#: lib/run-time.cpp:366
msgid "GNU standard options"
msgstr "GNU Standard Optionen"

#: lib/run-time.cpp:367
msgid "Standard options"
msgstr "Standard Optionen"

#: lib/run-time.cpp:417
msgid "display this help and exit"
msgstr "Diese Hilfe anzeigen und beenden"

#: lib/run-time.cpp:418
msgid "output version information and exit"
msgstr "Versionsinformationen ausgeben und beenden"

#: lib/tag.cpp:70
#, boost-format
msgid "Options provided by %1%."
msgstr "Optionen bereitgestellt von %1%."

#: lib/tag.cpp:74
msgid "General"
msgstr "Allgemein"

#: lib/tag.cpp:75
msgid "Basic options."
msgstr "Grundlegende Optionen."

#: lib/tag.cpp:79
msgid "Geometry"
msgstr "Geometrie"

#: lib/tag.cpp:80
msgid "Scan area and image size related options."
msgstr "Scanbereichsoptionen und Bildgrößenoptionen."

#: lib/tag.cpp:84
msgid "Enhancement"
msgstr "Optimierung"

#: lib/tag.cpp:85
msgid "Image modification options."
msgstr "Bildbearbeitungsoptionen."

#: sane/backend.cpp:1033
msgid ""
"The current locale settings are not supported by the standard C++ library "
"used by this application.  This is most likely caused by a misconfigured "
"locale but may also be due to use of a C++ library without localization "
"support.  You can work around this issue by starting the application in a \"C"
"\" locale, but you really should check your locale configuration and the "
"locale support of the C++ library used by the application."
msgstr ""

#: sane/backend.cpp:1057
#, fuzzy
msgid "library initialization failed"
msgstr "Kalibrierung fehlgeschlagen."

#: sane/backend.hpp:294
#, c-format, boost-format
msgid "Unknown SANE status code %d"
msgstr "Unbekannter SANE status code %d"

#: src/help.cpp:52
msgid "display help information for a command"
msgstr "Zeige Hilfe für einen Befehl"

#: src/list.cpp:50 src/main.cpp:66
msgid "list available image acquisition devices"
msgstr "Verfügbare Scanner auflisten"

#: src/main.cpp:61
msgid "Supported commands"
msgstr "Unterstützte Befehle"

#: src/main.cpp:64
msgid "display the help for a command and exit"
msgstr "Zeige Hilfe für einen Befehl und beende"

#: src/main.cpp:65
msgid "output command version information and exit"
msgstr "Ausgabe Information Befehlversion und beende"

#: src/main.cpp:67
msgid "scan with a suitable utility"
msgstr "Scanne mit einem geignetem Prgramm"

#: src/main.cpp:72
msgid "next generation image acquisition"
msgstr ""

#: src/scan-cli.cpp:163
#, boost-format
msgid "%1%: not found"
msgstr "%1%: nicht gefunden"

#: src/scan-cli.cpp:168 src/scan-cli.cpp:584
msgid "no usable devices available"
msgstr "Keine nutzbaren Geräte gefunden"

#: src/scan-cli.cpp:175
#, boost-format
msgid "%1%: found but has no driver"
msgstr "%1%: gefunden jedoch ohne Treiber"

#: src/scan-cli.cpp:187
#, boost-format
msgid "%1%: not supported"
msgstr "%1%: nicht unterstützt"

#: src/scan-cli.cpp:237
#, boost-format
msgid ""
"%1%\n"
"Allowed values: %2%"
msgstr ""
"%1%\n"
"Erlaubte Werte: %2%"

#: src/scan-cli.cpp:244
#, boost-format
msgid "Allowed values: %1%"
msgstr "Erlaubte Werte: %1%"

#: src/scan-cli.cpp:509
msgid "image acquisition device to use"
msgstr "Benutze Scanner"

#: src/scan-cli.cpp:511
msgid "output destination to use"
msgstr "Benutze Ausgabeziel"

#: src/scan-cli.cpp:525 src/scan-gtkmm.cpp:73
msgid "Utility options"
msgstr "Optionen Dienstprogramm"

#: src/scan-cli.cpp:528
msgid "log device I/O in hexdump format"
msgstr "log Gerät I/O im hexdump Format"

#: src/scan-cli.cpp:531
msgid ""
"output image format\n"
"PNM, PNG, JPEG, PDF, TIFF or one of the device supported transfer-formats.  "
"The explicitly mentioned types are normally inferred from the output file "
"name.  Some require additional libraries at build-time in order to be "
"available."
msgstr ""
"Bildausgabeformat\n"
"PNM, PNG, JPEG, PDF, TIFF oder ein vom Gerät unterstütztes Format. Die "
"genannten Formate werden üblicherweise von der Dateiendung abgeleitet. "
"Einige erfordern zusätzliche Bibliotheken beim erstellen des Programms "
"um verfügbar zu sein."

#: src/scan-cli.cpp:593
msgid "Device actions"
msgstr "Aktionen Gerät"

#: src/scan-cli.cpp:604
msgid ""
"Only perform the actions given on the command-line. Do not perform image "
"acquisition."
msgstr ""
"Nur die per Kommandozeile übergebenen Aktionen ausführen. Keine Bilddaten "
"einholen."

#: src/scan-cli.cpp:610
msgid "Device options"
msgstr "Optionen Gerät"

#: src/scan-cli.cpp:612
msgid "Add-on options"
msgstr "Optionen Erweiterung"

#: src/scan-cli.cpp:706
msgid ""
"Note: device options may be ignored if their prerequisites are not "
"satisfied.\n"
"A '--duplex' option may be ignored if you do not select the ADF, for "
"example.\n"
msgstr ""
"Hinweis: Geräteoptionen können ignoriert werden, wenn die Bedingungen "
"nicht erfüllt werden.\n"
"Eine '--duplex' Option wird bspw. ignoriert wenn nicht gleichzeitig der "
"ADF gewählt wird.\n"

#: src/scan-cli.cpp:879
#, boost-format
msgid "cannot infer image format from file extension: '%1%'"
msgstr "Kann Bildformat nicht von der Dateiendung ableiten: '%1%'"

#: src/scan-cli.cpp:900
#, boost-format
msgid "unsupported image format: '%1%'"
msgstr "Bildformat nicht unterstützt: '%1%'"

#: src/scan-cli.cpp:955
#, boost-format
msgid "%1% does not support multi-image files"
msgstr "%1% unterstützt nicht mehrere Bilder in einer Datei."

#: src/scan-gtkmm.cpp:77
msgid "use an alternative GUI layout definition file"
msgstr "Nutze eine alternative GUI Layout Definitions-Datei"

#: src/scan-gtkmm.cpp:79
msgid "use an alternative GUI resource file"
msgstr "Nutze eine alternative GUI Hilfs-Datei"

#: src/scan.cpp:77
msgid "Command options"
msgstr "Befehlsoptionen"

#: src/scan.cpp:82
msgid ""
"Start an interactive user interface\n"
"The default behavior depends on the environment where one runs the command.  "
"A scan utility suitable for non-interactive use can be selected with the '--"
"no-interface' option."
msgstr ""

#: src/scan.cpp:91
msgid "acquire images with a suitable utility"
msgstr "Bilder mit einem geeigneten Dienstprogramm scannen"

#: src/version.cpp:52
msgid "display version information for a command"
msgstr "Zeige Versionsinformation für einen Befehl"
